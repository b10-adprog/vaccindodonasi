package cs.ui.ac.id.vaccindodonasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccindoDonasiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaccindoDonasiApplication.class, args);
    }

}
