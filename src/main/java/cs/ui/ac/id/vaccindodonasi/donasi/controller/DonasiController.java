package cs.ui.ac.id.vaccindodonasi.donasi.controller;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;
import cs.ui.ac.id.vaccindodonasi.donasi.service.DonasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class DonasiController {

    @Autowired
    DonasiService donasiService;

    @GetMapping(path = "/")
    public String home(Model model) {
        List<Donasi> donasiList = donasiService.getAllDonasi();
        model.addAttribute("listDonasi", donasiList);
        model.addAttribute("totalDonasi", donasiService.countTotalDonasi());
        return "landingPage";
    }

    @GetMapping(path = "/donasi/input/")
    public String inputDonasi(Model model) {
        model.addAttribute("donasi", new Donasi());
        return "donasi/tambahDonasi";
    }

    @PostMapping(path = "/donasi/inputSave/")
    public String inputDonasi(
            @Valid @ModelAttribute(name = "donasi") Donasi donasi,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "donasi/tambahDonasi";
        }
        donasiService.addDonasi(donasi);
        return "redirect:/donasi/input/";
    }
}
