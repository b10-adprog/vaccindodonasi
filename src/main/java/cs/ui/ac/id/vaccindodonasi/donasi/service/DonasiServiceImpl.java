package cs.ui.ac.id.vaccindodonasi.donasi.service;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;
import cs.ui.ac.id.vaccindodonasi.donasi.repository.DonasiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DonasiServiceImpl implements DonasiService {

    @Autowired
    DonasiRepository donasiRepository;

    @Override
    public List<Donasi> getAllDonasi() {
        List<Donasi> listDonasi = donasiRepository.findAll();
        return listDonasi;
    }

    @Override
    @Transactional
    public void addDonasi(Donasi donasi) {
        donasiRepository.save(donasi);
        countTotalDonasi();
    }

    @Override
    public int countTotalDonasi() {
        List<Donasi> donasiList = getAllDonasi();
        int totalDonasi = 0;
        for (int a = 0; a < donasiList.size(); a++) {
            totalDonasi += donasiList.get(a).getJumlahDonasi();
        }
        return totalDonasi;
    }
}
