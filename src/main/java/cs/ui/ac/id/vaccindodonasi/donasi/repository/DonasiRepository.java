package cs.ui.ac.id.vaccindodonasi.donasi.repository;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonasiRepository extends JpaRepository<Donasi, Integer>  {

    @Override
    List<Donasi> findAll();
}
