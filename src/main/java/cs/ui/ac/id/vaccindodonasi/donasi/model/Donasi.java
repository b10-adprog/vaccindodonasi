package cs.ui.ac.id.vaccindodonasi.donasi.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "donasi")
@Data
@NoArgsConstructor
public class Donasi {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_donasi")
    private Integer idDonasi;

    @NotEmpty(message = "Nama donatur tidak boleh kosong.")
    @Column(updatable = false, nullable = false)
    private String donaturName;

    @NotNull(message = "Jumlah donasi tidak boleh kosong.")
    @Column(updatable = false, nullable = false)
    private int jumlahDonasi;

    public Donasi(String donaturName, int jumlahDonasi) {
        this.donaturName = donaturName;
        this.jumlahDonasi = jumlahDonasi;
    }
}
