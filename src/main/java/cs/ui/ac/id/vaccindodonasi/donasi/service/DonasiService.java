package cs.ui.ac.id.vaccindodonasi.donasi.service;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;

import java.util.List;

public interface DonasiService {
    List<Donasi> getAllDonasi();
    void addDonasi(Donasi donasi);
    int countTotalDonasi();
}
