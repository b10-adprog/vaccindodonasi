package cs.ui.ac.id.vaccindodonasi.donasi.service;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;
import cs.ui.ac.id.vaccindodonasi.donasi.repository.DonasiRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DonasiServiceTest {

    @Mock
    DonasiRepository donasiRepository;

    @InjectMocks
    DonasiServiceImpl donasiService;

    private Donasi donasi;
    private List<Donasi> donasiList;

    @BeforeEach
    public void setUp() {
        donasi = new Donasi();
        donasi.setIdDonasi(1);
        donasi.setDonaturName("Mutia Rahmatun Husna");
        donasi.setJumlahDonasi(1000000);

        donasiList = new ArrayList<>();
        donasiList.add(donasi);
    }

    @Test
    public void getAllDonasi() {
        when(donasiService.getAllDonasi()).thenReturn(donasiList);

        List<Donasi> listDonasiFromMock = donasiService.getAllDonasi();

        assertEquals(listDonasiFromMock.size(), 1);
        verify(donasiRepository, times(1)).findAll();
    }

    @Test
    public void addDonasi() {

        Donasi donasiDummy = new Donasi();
        donasiDummy.setIdDonasi(1);
        donasiDummy.setDonaturName("Melati Sukma");
        donasiDummy.setJumlahDonasi(250000);

        donasiService.addDonasi(donasiDummy);

        verify(donasiRepository, times(1)).save(any(Donasi.class));
    }

    @Test
    public void countTotalDonasi() {
    }
}