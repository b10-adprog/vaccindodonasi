package cs.ui.ac.id.vaccindodonasi.donasi.controller;

import cs.ui.ac.id.vaccindodonasi.donasi.model.Donasi;
import cs.ui.ac.id.vaccindodonasi.donasi.service.DonasiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DonasiController.class)
public class DonasiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DonasiService donasiService;

    @Test
    public void getHomePage() throws Exception {
        Donasi donasi = new Donasi("Mutia Rahmatun Husna", 150000);
        donasi.setIdDonasi(1);
        assertNotNull(donasi);

        List<Donasi> donasiList = new ArrayList<>();
        donasiList.add(donasi);
        int totalDonasi = 0;

        when(donasiService.getAllDonasi()).thenReturn(donasiList);
        when(donasiService.countTotalDonasi()).thenReturn(totalDonasi);

        mockMvc.perform(get("/")
                .param("listDonasi", String.valueOf(donasiList))
                .param("totalDonasi", String.valueOf(totalDonasi))
        ).andExpect(status().isOk())
                .andExpect(model().attributeExists("listDonasi"))
                .andExpect(model().attributeExists("totalDonasi"))
                .andExpect(handler().methodName("home"))
                .andExpect(view().name("landingPage"));
    }

    @Test
    public void getInputDonasiForm() throws Exception {
        Donasi donasi = new Donasi();

        mockMvc.perform(get("/donasi/input/")
                .param("donasi", String.valueOf(donasi))
        ).andExpect(status().isOk())
                .andExpect(model().attributeExists("donasi"))
                .andExpect(handler().methodName("inputDonasi"))
                .andExpect(view().name("donasi/tambahDonasi"));
    }

    @Test
    public void postInputDonasiRedirectToHomePage() throws Exception {

        Donasi donasi = new Donasi("Mutia Rahmatun Husna", 150000);
        donasi.setIdDonasi(1);
        assertNotNull(donasi);

        List<Donasi> donasiList = new ArrayList<>();
        donasiList.add(donasi);

        when(donasiService.getAllDonasi()).thenReturn(donasiList);

        mockMvc.perform(post("/donasi/inputSave/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .param("donaturName", donasi.getDonaturName())
                .param("jumlahDonasi", String.valueOf(donasi.getJumlahDonasi()))
        ).andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("inputDonasi"))
                .andExpect(redirectedUrl("/donasi/input/"));
        verify(donasiService, times(1)).addDonasi(any(Donasi.class));

    }
}